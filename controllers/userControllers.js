const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// bcrypt is a package which allows us to hash our passwords to add a layer of security 

// Check if the email already exists
/*
Steps
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to User Registration

Setps:
1. Create a new User object using the mongoose model and the info from the reqBody
2. Make sure that the password is encrypted
3. Save the new User to the database
send a response back to the FE application based on the result of the "find" method
*/


module.exports.checkEmailExists =(reqBody)=>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.legth>0){
			return true;
		}else{
			return false;
		};
	});
};

// User Registration
/*
Setps:
	1. Create a new User object using the mongoose model and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// password: reqBody.password
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// User Authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};

module.exports.getProfile = (reqBody) =>{
	return User.findByIdAndUpdate(reqBody.id, { password: "" }).then(result=>{
			return result;
});
};


/*
{
    "firstName":"John",
    "lastName": "Smith",
    "email" : "john@mail.com",
    "mobileNo": "09123456789",
    "password":"john1234"
}
*/