/*
	auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our app
*/

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens
/*
	-JSON Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
*/

module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		// email: user.email,
		// isAdmin: user.isAdmin
	};
	return jwt.sign(data,secret,{});
};