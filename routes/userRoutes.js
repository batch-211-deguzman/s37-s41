const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

//Route for checking if the user's email already exists in the database
//pass the "body" property of our request

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

router.post("/details",(req,res)=>{
	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
});


module.exports = router;